//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    
    var rabbit64:SKSpriteNode!
    var wall: SKSpriteNode!
    var isblock1: SKSpriteNode!
     var isblock2: SKSpriteNode!
    var winblock: SKSpriteNode!
    var stopblock: SKSpriteNode!
    var flagblock: SKSpriteNode!
    var flag: SKSpriteNode!
    var wallblock: SKSpriteNode!
    let PLAYER_SPEED:CGFloat = 20
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        // initialze the player
                self.rabbit64 = self.childNode(withName: "rabbit64") as! SKSpriteNode
               self.rabbit64.physicsBody = SKPhysicsBody(rectangleOf: rabbit64.size)
        self.rabbit64.physicsBody?.affectedByGravity = false
        self.rabbit64.physicsBody?.allowsRotation = false
        self.rabbit64.physicsBody?.categoryBitMask = 1
        
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
                     self.flag.physicsBody = SKPhysicsBody(rectangleOf: flag.size)
              self.flag.physicsBody?.affectedByGravity = false
              self.flag.physicsBody?.allowsRotation = false
              self.flag.physicsBody?.categoryBitMask = 1
        
//                 self.wall.physicsBody?.collisionBitMask = 0
//                self.wall.physicsBody?.contactTestBitMask = 0
        
        self.enumerateChildNodes(withName: "wall") {
                   (node, stop) in
            self.wall = node as! SKSpriteNode
            self.wall.physicsBody = SKPhysicsBody(rectangleOf: self.wall.size)
                           self.wall.physicsBody?.affectedByGravity = false
                            self.wall.physicsBody?.isDynamic = false
                           self.wall.physicsBody?.categoryBitMask = 0
                            self.wall.physicsBody?.collisionBitMask = 0
                           self.wall.physicsBody?.contactTestBitMask = 0
        }
        
        
//        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
//                self.wall.physicsBody = SKPhysicsBody(rectangleOf: wall.size)
//                self.wall.physicsBody?.affectedByGravity = false
//                 self.wall.physicsBody?.isDynamic = false
//                self.wall.physicsBody?.categoryBitMask = 4
//                 self.wall.physicsBody?.collisionBitMask = 0
//                self.wall.physicsBody?.contactTestBitMask = 0
        
       
        
        self.isblock1 = self.childNode(withName: "isblock1") as! SKSpriteNode
        self.isblock1.physicsBody = SKPhysicsBody(rectangleOf: isblock1.size)
                self.isblock1.physicsBody?.affectedByGravity = false
        self.isblock1.physicsBody?.isDynamic = false
                self.isblock1.physicsBody?.categoryBitMask = 8
                 self.isblock1.physicsBody?.collisionBitMask = 0
                self.isblock1.physicsBody?.contactTestBitMask = 0
        
        self.isblock2 = self.childNode(withName: "isblock2") as! SKSpriteNode
               self.isblock2.physicsBody = SKPhysicsBody(rectangleOf: isblock2.size)
                       self.isblock2.physicsBody?.affectedByGravity = false
               self.isblock2.physicsBody?.isDynamic = false
                       self.isblock2.physicsBody?.categoryBitMask = 8
                        self.isblock2.physicsBody?.collisionBitMask = 0
                       self.isblock2.physicsBody?.contactTestBitMask = 0
        
        self.winblock = self.childNode(withName: "winblock") as! SKSpriteNode
               self.winblock.physicsBody = SKPhysicsBody(rectangleOf: winblock.size)
                       self.winblock.physicsBody?.affectedByGravity = false
                self.rabbit64.physicsBody?.allowsRotation = false
                       self.winblock.physicsBody?.categoryBitMask = 2
//                        self.winblock.physicsBody?.collisionBitMask = 1
//                       self.winblock.physicsBody?.contactTestBitMask = 1
        
        self.stopblock = self.childNode(withName: "stopblock") as! SKSpriteNode
        self.stopblock.physicsBody = SKPhysicsBody(rectangleOf: stopblock.size)
                self.stopblock.physicsBody?.affectedByGravity = false
          self.stopblock.physicsBody?.allowsRotation = false
                self.stopblock.physicsBody?.categoryBitMask = 2
//                 self.stopblock.physicsBody?.collisionBitMask = 1
//                self.stopblock.physicsBody?.contactTestBitMask = 1
        
        
        self.flagblock = self.childNode(withName: "flagblock") as! SKSpriteNode
        self.flagblock.physicsBody = SKPhysicsBody(rectangleOf: flagblock.size)
                self.flagblock.physicsBody?.affectedByGravity = false
          self.flagblock.physicsBody?.allowsRotation = false
                self.flagblock.physicsBody?.categoryBitMask = 2
//                 self.flagblock.physicsBody?.collisionBitMask = 0
//                self.flagblock.physicsBody?.contactTestBitMask = 0

       self.wallblock = self.childNode(withName: "wallblock") as! SKSpriteNode
       self.wallblock.physicsBody = SKPhysicsBody(rectangleOf: wallblock.size)
               self.wallblock.physicsBody?.affectedByGravity = false
          self.rabbit64.physicsBody?.allowsRotation = false
               self.wallblock.physicsBody?.categoryBitMask = 2
//                self.wallblock.physicsBody?.collisionBitMask = 0
//               self.wallblock.physicsBody?.contactTestBitMask = 0
        
    }
   
    
        func didBegin(_ contact: SKPhysicsContact) {
//               let nodeA = contact.bodyA.node
//               let nodeB = contact.bodyB.node
//               // if the condition is "FLAG IS WIN" then if()
//               if (nodeA == nil || nodeB == nil) {
//                   return
//               }
//
//               if (nodeA!.name == "rabbit64" && nodeB!.name == "flag") {
//
//                   print("Flag condition is working")
//
//               }
//               if (nodeA!.name == "flag" && nodeB!.name == "rabbit64") {
//                   print("Flag condition is working")
//                  // Similarly we can check if there are any other collisions taken place or not
//
//               }
//
//
        print("Something has collided!")
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if (wallblock.frame.intersects(isblock1.frame)==true) {
            if (isblock1.frame.intersects(stopblock.frame)==true) {
            print("All are together")
                self.enumerateChildNodes(withName: "wall") {
                                  (node, stop) in
                    self.wall = node as! SKSpriteNode
                      self.wall.physicsBody?.categoryBitMask = 8
                }
                
            
                
            }
            else
            {
                self.enumerateChildNodes(withName: "wall") {
                                                 (node, stop) in
                                   self.wall = node as! SKSpriteNode
                                     self.wall.physicsBody?.categoryBitMask = 0
            }
        }
    }
        else{
            self.enumerateChildNodes(withName: "wall") {
                                                            (node, stop) in
                                              self.wall = node as! SKSpriteNode
                                                self.wall.physicsBody?.categoryBitMask = 0
                       }
        }
        
        
        if (flagblock.frame.intersects(isblock2.frame)==true) {
                   if (isblock2.frame.intersects(winblock.frame)==true) {
                   print("All are together 2")
                    if (rabbit64.frame.intersects(flag.frame) == true) {
                        let winscreen = winning(size: self.size)
                                  self.view?.presentScene(winscreen)
                            
                    }
                   }
                  
           }
              
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // GET THE POSITION WHERE THE MOUSE WAS CLICKED
        // ---------------------------------------------
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)

        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "up") {
            // move up
            self.rabbit64.position.y = self.rabbit64.position.y + PLAYER_SPEED
        }
        else if (nodeTouched == "down") {
            // move down
             self.rabbit64.position.y = self.rabbit64.position.y - PLAYER_SPEED
        }
        else if (nodeTouched == "left") {
            // move left
             self.rabbit64.position.x = self.rabbit64.position.x - PLAYER_SPEED
        }
        else if (nodeTouched == "right") {
            // move right
             self.rabbit64.position.x = self.rabbit64.position.x + PLAYER_SPEED
        }
        
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
