//
//  winning.swift
//  BabaIsYou-F19
//
//  Created by yaad on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
class winning : SKScene{
   
      
         override init(size: CGSize) {
             super.init(size: size)
         }
         required init?(coder aDecoder: NSCoder) {
             fatalError("init(coder:) has not been implemented")
         }
         override func didMove(to view: SKView) {
             // SPRITE SETUP
             let background = SKSpriteNode(imageNamed:"winscreen")
             // put backgound in the middle of screen
             background.position = CGPoint(x:size.width/2, y:size.height/2)
             addChild(background)
         }
}

